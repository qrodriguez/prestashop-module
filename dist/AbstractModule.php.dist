<?php

declare(strict_types=1);

namespace {{ NAMESPACE }};

use PrestaShopBundle\Entity\Repository\TabRepository;
use Symfony\Component\Yaml\Yaml;

abstract class AbstractModule extends Module
{

    private string $configPath;
    private string $hookPath;
    private string $tabPath;

    /**
     * Constructor AbstractModule
     *
     * @param string $name
     * @param string $displayName
     * @param string $description
     * @param string $author
     */
    public function __construct(string $name, string $displayName, string $description, string $author)
    {
        $this->name          = $name;
        $this->version       = "1.0.0";
        $this->author        = $author;
        $this->need_instance = 0;
        $this->bootstrap     = true;
        $this->tab           = "others";

        $this->displayName   = $this->trans($displayName, [], "Admin.Globals");
        $this->description   = $this->trans($description, [], "Admin.Globals");

        $this->configPath    = _PS_ROOT_DIR_ . "/app/config/addons/$this->module.yml";
        $this->hookPath      = $this->getLocalPath()."config/hooks.yml"; 
        $this->tabPath       = $this->getLocalPath()."config/tabs.yml";

        $this->ps_versions_compliancy = [
            "min" => "1.7.7",
            "max" => _PS_VERSION_
        ];

    }

    /**
     * Install module
     *
     * @return bool
     */
    public function install(): bool
    {
        return parent::install()
            && $this->installConfig()
            && $this->installTabs()
            && $this->installHooks()
            && $this->executeSQL("install");
    }

    /**
     * Uninstall module
     *
     * @return bool
     */
    public function uninstall(): bool
    {
        return parent::uninstall()
            && $this->uninstallConfig()
            && $this->uninstallTabs()
            && $this->uninstallHooks()
            && $this->executeSQL("uninstall");
    }

    /**
     * Enable module
     *
     * @param bool $force_all 
     *
     * @return bool
     */
    public function enable($force_all = false): bool
    {
        return parent::enable($force_all)
            && $this->installConfig()
            && $this->installTabs()
            && $this->installHooks();
    }

    /**
     * Disable module
     *
     * @param bool $force_all
     *
     * @return bool
     */
    public function disable($force_all = false): bool
    {
        return parent::disable($force_all)
            && $this->uninstallConfig()
            && $this->uninstallTabs()
            && $this->uninstallHooks();
    }

    /**
     * Execute SQL request for install or uninstall tables in a module
     *
     * @return bool
     */
    protected function executeSQL(string $type): bool
    {
        $files = glob($this->getLocalPath()."sql/$type/*.sql");

        foreach ($files as $file) {
            $sql = str_replace(
                ["PREFIX_", "ENGINE_TYPE"],
                [_DB_PREFIX_, _MYSQL_ENGINE_],
                file_get_contents($file)
            );

            Db::getInstance()->execute($sql);
        }

        return true;
    }

    /**
     * Install config for a module
     *
     * @return bool
     */
    protected function installConfig(): bool
    {
        $config = [
            "imports" => [
                [
                   "resource" => "../../../modules/$this->module/config/services.yml" 
                ]
            ]
        ];

        $file = fopen($this->configPath, "w+");
        fwrite($file, Yaml::dump($config));
        return fclose($file);
    }

    /**
     * Uninstall config for a module
     * 
     * @return bool
     */
    protected function uninstallConfig(): bool 
    {
        if (!file_exists($this->configPath)) {
            return true;
        }

        return unlink($this->configPath);
    }
 
    /**
     * Install all tabs for a module
     *
     * @return bool
     */
    public function installTabs(): bool
    {
        if (!file_exists($this->tabPath)) {
            return true;
        }

        $tabs = Yaml::parseFile($this->tabPath)["tabs"] ?? [];

        foreach ($tabs as $className => $tab) {
            if (!$this->registerTab($className, $tab)) {
                return false; 
            }
        }
        
        return true;
    }

    /**
     * Uninstall all tabs for module
     *
     * @return bool
     */
    protected function uninstallTabs(): bool
    {
        if (!file_exists($this->tabPath)) {
            return true;
        }

        $tabs = Yaml::parseFile($this->tabPath)["tabs"] ?? [];

        foreach ($tabs as $tab) {
            if ($this->unregisterTab($tab)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Install hooks for a module
     *
     * @return bool
     */
    protected function installHooks(): bool
    {
        if (!file_exists($this->hookPath)) {
            return true;
        }

        $hooks = Yaml::parseFile($this->hookPath)["hooks"] ?? [];

        foreach ($hooks as $hook) {
            if (!$this->registerHook($hook)) {
                return false;
            }
        }
    }

    /**
     * Uninstall hooks for a module
     *
     * @return bool
     */
    protected function uninstallHooks(): bool
    {
        if (!file_exists($this->hookPath)) {
            return true;
        }

        $hooks = Yaml::parseFile($this->hookPath)["hooks"] ?? [];

        foreach ($hooks as $hook) {
            if (!$this->unregisterHook($hook)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Register a tab for back-office
     * 
     * @param array $tab
     * 
     * @return bool
     */
    private function registerTab(array $tab): bool
    {
        /** @var TabRepository $repo */
        $repo = $this->get("prestashop.core.admin.tab.repository");
        $tab  = new Tab($repo->findOneIdByClassName($tab["class_name"]));
        
        $tab->active     = 1;
        $tab->class_name = $tab["class_name"];
        $tab->route_name = $tab["route_name"];
        $tab->icon       = $tab["icon"] ?? "extension";
        $tab->id_parent  = $repo->findOneIdByClassName($tab["parent_name"]);
        $tab->module     = $this->name;

        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang["lang_id"]] = $this->trans($tab["name"], [], "Admin.Globals", $lang["locale"]);
        }

        try {
            return $tab->save();
        } catch (PrestaShopException $e) {
            return false;
        }
    }

    /**
     * Unregister a tab for back-office
     * 
     * @param array $tab
     *
     * @return bool
     */
    private function unregisterTab(array $tab): bool
    {
        /** @var TabRepository $repo */
        $repo = $this->get("prestashop.core.admin.tab.repository");
        $tab  = new Tab($repo->findOneIdByClassName($tab["class_name"]));
        
        try {
            return $tab->delete();
        } catch(PrestaShopException $e) {
            return false;
        }
    }

}